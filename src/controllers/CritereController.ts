import { Request, Response } from "express-serve-static-core";

import { db } from "../server";
import CompetenceController from "./CompetenceController";

export default class CritereController {
    static checkbox(req: Request, res: Response): void {

        //const user = db.prepare("INSERT INTO user (username, password) VALUES ('SuperTesteur', 'az')").run();
        //console.log(req.body.checkbox)
        let recevu = req.body.checkbox;
        console.log(recevu)
        //const wiper = db.prepare('DELETE FROM user_has_criteres WHERE user_id = 1').run()

        let pivottbl = db.prepare('SELECT criteres_id FROM user_has_criteres WHERE user_id = 1').all();
        pivottbl = pivottbl.map(e => Number(e.criteres_id));

        const id = req.params.id;
        const critComp1db = db.prepare('SELECT * FROM criteres WHERE competences_id = ?').all(id);
        //let critCompID = db.prepare('SELECT ("id") FROM criteres WHERE competences_id = ?').all(id)
        let critCompID = critComp1db.map(e => Number(e.id));

        if (recevu == null) {
            const nul = db.prepare('DELETE FROM user_has_criteres WHERE user_id = 1').run()
        } else if (typeof Number(recevu) == 'number') {

            const addsolo = db.prepare('INSERT INTO user_has_criteres ("user_id", "criteres_id") VALUES (1, ?)').run(recevu);

            for (let i = 0; i < critCompID.length; i++) {
                if (pivottbl.includes(critCompID[i]) && recevu != critCompID[i]) {
                    const nulll = db.prepare('DELETE FROM user_has_criteres WHERE user_id = 1 AND criteres_id = ?').run(critCompID[i])
                }
            }
        }
        else {
            recevu = recevu.map((e: any) => Number(e));

            critComp1db.forEach(element => {
                //console.log(element.id)
                // if(pivottbl.includes(element.id) && recevu.includes(element.id)){
                //    // console.log("pas add pck deja add")
                // }
                // else 
                if (!pivottbl.includes(element.id) && recevu.includes(element.id)) {
                    // console.log("le add")
                    const hope = db.prepare('INSERT INTO user_has_criteres ("user_id", "criteres_id") VALUES (1, ?)').run(element.id);
                }
                else if (pivottbl.includes(element.id) && !recevu.includes(element.id)) {
                    //console.log("le delete")
                    const wiper = db.prepare('DELETE FROM user_has_criteres WHERE criteres_id = ? AND user_id = 1 ').run(element.id)
                }
                // else if(!pivottbl.includes(element.id) && !recevu.includes(element.id)){
                //    // console.log("pas add pck pas reçu")
                // }
            });
        }
        //  console.log(pivottbl)

        CompetenceController.competence(req, res)
    }

    /**
     * Show criteres form
     * @param req 
     * @param res 
     */
    static critere(req: Request, res: Response): void {
        const firstUser = db.prepare('SELECT * FROM user WHERE id = 1').all();

        const id = req.params.id;
        const critereFromCompetenceId = db.prepare('SELECT * FROM criteres WHERE competences_id = ?').all(id);

        let checker = db.prepare('SELECT criteres_id FROM user_has_criteres WHERE user_id = 1').all();
        checker = checker.map(e => Number(e.criteres_id));
        //console.log(checker)
        critereFromCompetenceId.forEach(critere => {
            //console.log(critere.id)
            if (checker.includes(critere.id)) {
                critere.checked = "checked";
            }
            else {
                critere.checked = "";
            }
        })
        //console.log(critComp1db)
        res.render('pages/critere', {
            title: 'critere',
            critere: critereFromCompetenceId,
            index: id
        });
    }
}